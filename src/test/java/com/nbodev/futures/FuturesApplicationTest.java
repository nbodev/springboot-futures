package com.nbodev.futures;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import com.nbodev.futures.controller.FuturesController;
import com.nbodev.futures.sample.ISample;
import com.nbodev.futures.sample.TaskResult;

@SpringBootTest(classes = FuturesApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
class FuturesApplicationTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void testCompletableFutures() {

		final ResponseEntity<TaskResult[]> response = this.restTemplate.getForEntity("http://localhost:" + port + FuturesController.COMPLETABLE_FUTURES_URL,
				TaskResult[].class);

		assertion(response);

	}

	@Test
	void testFutures() {

		final ResponseEntity<TaskResult[]> response = this.restTemplate.getForEntity("http://localhost:" + port + FuturesController.FUTURES_URL,
				TaskResult[].class);

		assertion(response);

	}

	private void assertion(ResponseEntity<TaskResult[]> response) {
		assertEquals(response.getBody().length, ISample.ITEMS.size());

		for (final TaskResult result : response.getBody()) {

			if (null != result) {
				assertNotEquals("C", result.getItem());
				assertTrue(ISample.ITEMS.contains(result.getItem()));
			}
		}
	}

}
