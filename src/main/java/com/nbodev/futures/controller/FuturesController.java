package com.nbodev.futures.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nbodev.futures.sample.SampleCompletableFuture;
import com.nbodev.futures.sample.SampleFutures;
import com.nbodev.futures.sample.TaskResult;

@RestController
public class FuturesController {
	public static final String FUTURES_URL = "/futures";
	public static final String COMPLETABLE_FUTURES_URL = "/completable-futures";

	@GetMapping("/hello")
	public ResponseEntity<String> hello() {

		try {
			return new ResponseEntity<>(LocalDateTime.now().toString(), HttpStatus.OK);
		} catch (final Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(COMPLETABLE_FUTURES_URL)
	public ResponseEntity<List<TaskResult>> completableFutures() {

		try {
			return new ResponseEntity<>(new SampleCompletableFuture().process(), HttpStatus.OK);
		} catch (final Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(FUTURES_URL)
	public ResponseEntity<List<TaskResult>> futures() {

		try {
			return new ResponseEntity<>(new SampleFutures().process(), HttpStatus.OK);
		} catch (final Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
