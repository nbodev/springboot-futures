package com.nbodev.futures.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleFutures implements ISample {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		final var sample = new SampleFutures();
		sample.process();
	}

	@Override
	public List<TaskResult> process() {
		// Create the executor service
		final var executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		// Rely on a CompletionService in order to submit the tasks,
		// CompletionService gives the first finished task and does not care about the submission order
		final CompletionService<TaskResult> ecs = new ExecutorCompletionService<>(executor);

		// Submit the tasks
		for (final String item : ITEMS) {
			final var myTask = new MyTask(item);
			ecs.submit(myTask);
		}

		// Results
		final List<TaskResult> results = new ArrayList<>();

		// Get the results as far as they are available
		for (var i = 0; i < ITEMS.size(); i++) {

			try {
				final var taskResult = ecs.take().get();
				logger.info("Result {}.", taskResult);
				results.add(taskResult);
			} catch (final Exception e) {
				logger.error("Exception message has been raised {}.", e.getMessage());
				results.add(null);// null taskResult when it fails
			}
		}

		// Always reclaim resources
		logger.info("Shutting down the executor pool.");
		executor.shutdown();
		logger.info("Executor pool down.");

		return results;
	}

}

final class MyTask implements Callable<TaskResult> {
	private final String item;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public MyTask(String item) {
		this.item = item;
	}

	@Override
	public TaskResult call() throws Exception {
		final var start = System.currentTimeMillis();
		final var sleepingTime = ThreadLocalRandom.current().nextLong(5_000);
		logger.info("Item {}, sleepingTime {}.", item, sleepingTime);

		// we raise and exception on purpose
		if (item.equals("C")) {
			throw new IllegalStateException("Exception related to item " + item);
		}
		Thread.sleep(sleepingTime);
		final var precessingTime = System.currentTimeMillis() - start;
		return new TaskResult(item, precessingTime);
	}
}
