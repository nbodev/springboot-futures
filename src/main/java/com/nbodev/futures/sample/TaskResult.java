package com.nbodev.futures.sample;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class TaskResult {
	private final String item;
	private final long precessingTime;

}
