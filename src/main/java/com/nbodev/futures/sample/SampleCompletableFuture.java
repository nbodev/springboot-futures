package com.nbodev.futures.sample;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SampleCompletableFuture implements ISample {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		final var sample = new SampleCompletableFuture();
		sample.process();
	}

	@Override
	public List<TaskResult> process() {
		// Create the executor service
		final var executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		final List<TaskResult> results = new ArrayList<>();
		final CompletableFuture<TaskResult>[] futuresList = new CompletableFuture[ITEMS.size()];
		final var atomicInt = new AtomicInteger(0);

		// Submit the tasks
		for (final String item : ITEMS) {
			submitItems(item, executor, futuresList, atomicInt);
		}

		// In case we need to wait for all the futures to finish we join
		// with CompletableFuture.allOf
		try {
			CompletableFuture.allOf(futuresList).join();

			for (final CompletableFuture<TaskResult> future : futuresList) {
				results.add(future.get());
			}
		} catch (final Exception e) {
			logger.error("Exception when doing the join {}.", e.getMessage());

		}

		// Always reclaim resources
		logger.info("Shutting down the executor pool.");
		executor.shutdown();
		logger.info("Executor pool down.");

		return results;

	}

	private void submitItems(String item, ExecutorService executor, CompletableFuture<TaskResult>[] futuresList, AtomicInteger atomicInt) {
		final var future = CompletableFuture
				.supplyAsync(() -> {

					try {
						return processItem(item);
					} catch (final Exception e) {
						// re throw so handle gets the exception
						throw new CompletionException(e.getMessage(), e);
					}
				}, executor)
				.handle((result, exception) -> {

					if (exception != null) {
						logger.error("Exception handle, message: {}.", exception.getMessage());
						return null;// null taskResult when it fails

					} else {
						logger.info("No exception for item {}, processing time {}.", result.getItem(), result.getPrecessingTime());
						return result;
					}
				});
		// this futuresList is not mandatory but used as an example for joining and looping over all the results
		futuresList[atomicInt.get()] = future;
		atomicInt.incrementAndGet();
	}

	private TaskResult processItem(String item) throws Exception {
		final var start = System.currentTimeMillis();
		final var sleepingTime = ThreadLocalRandom.current().nextLong(5_000);
		logger.info("Item {}, sleepingTime {}.", item, sleepingTime);

		if (item.equals("C")) {
			throw new IllegalStateException("Exception related to item " + item);
		}

		Thread.sleep(sleepingTime);

		final var precessingTime = System.currentTimeMillis() - start;
		return new TaskResult(item, precessingTime);
	}
}
