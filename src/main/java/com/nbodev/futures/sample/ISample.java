package com.nbodev.futures.sample;

import java.util.List;

public interface ISample {
	List<String> ITEMS = List.of("A", "B", "C", "D", "E", "F", "G");

	List<TaskResult> process();
}
