# SpringBoot CompletableFuture Vs Futures


## CompletableFuture
- we process items, raise an exception and use `.handle((result, exception)` in order recover from the exception. 
- we use `CompletableFuture.allOf(futuresList).join();` so then we wait for all the results and return them once all done, this is not mandatory. 
- CompletableFuture is a non blocking use case.
- `SampleCompletableFuture` run this class with its main.

## Futures
- classic `CompletionService` submitting tasks (CompletionService gives the first finished task and does not care about the submission order).
- we use `final var taskResult = ecs.take().get();` in order to get the first finished items.
- Future is a blocking use case.
- `SampleFutures` run this class with its main.

## Run the fat jar
- compile with `mvn clean install`
- run `java -jar ./target/futures-0.0.1-SNAPSHOT.jar`

## Run in the IDE
- run the `FuturesApplication` class

##  Visit those links in your browser in order to run the examples
- http://localhost:8080/futures
- http://localhost:8080/completable-futures

## Links
```
https://www.callicoder.com/java-8-completablefuture-tutorial/
https://mincong.io/2020/05/30/exception-handling-in-completable-future/
https://www.logicbig.com/tutorials/core-java-tutorial/java-multi-threading/completion-stages-exception-handling.html`
````